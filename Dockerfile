FROM python:3.10.3-buster

WORKDIR /app

RUN pip install pipenv
COPY ./Pipfile ./
RUN pipenv install

COPY . .

CMD ["pipenv", "run", "invoke", "prod"]
