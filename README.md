# Symbolic Form (API)

## Requirements

Berikut adalah list requirements yang diperlukan untuk dapat menginstall & menjalankan project:

1. Python (>= 3.10.x)
2. pip (untuk menginstall pipenv)
3. pipenv
4. RDBMS (disarankan MySQL)
5. Docker (jika ingin menjalankan dalam mode production)

## Instalasi

1. Install dependencies

   ```bash
   # di sini digunakan flag `--dev` karena secara default
   # pipenv tidak menginstall dev-dependencies
   pipenv install --dev
   ```

2. Buat file `.env` di project root. Contoh konten untuk file ini bisa dilihat di dalam file `.env.example`. Sesuaikan dengan konfigurasi masing-masing (misal RDBMS yang digunakan adalah PostgreSQL dsb).

3. Jalankan migration

   ```bash
   pipenv run invoke migration.up
   ```

4. Nyalakan server:

   ```bash
   # Untuk development, jalankan:
   pipenv run invoke dev --reload

   # Jika hanya ingin menyalakan server (tanpa fitur hot-reload):
   pipenv run invoke dev
   ```

5. Ketik alamat `localhost:8000/docs` di browser untuk melihat API endpoint yang tersedia

## Tips

- Untuk menjalankan test, jalankan command:

  ```bash
  pipenv run invoke test
  ```

- Jika ingin mengganti RDBMS, install driver yang diperlukan terlebih dulu. Di project ini driver untuk SQLite dan MySQL sudah terinstall sejak awal. Jika ingin mengganti, misal ke PostgreSQL, install driver tanpa perlu memasukkannya ke dalam file `requirements.txt`:

  ```bash
  # contoh menggunakan PostgreSQL, langsung jalankan
  pipenv install psycopg2
  ```

  Lihat [dokumentasi](https://docs.sqlalchemy.org/en/14/core/engines.html#supported-databases) untuk selengkapnya

- Alembic sudah terkonfigurasi untuk mendeteksi secara otomatis perubahan-perubahan yang ada di dalam tiap model yang teregistrasi. Sehingga setelah membuat / mengupdate model, bisa langsung jalankan perintah berikut untuk membuat file migration yang baru:

  ```bash
  pipenv run invoke migration.generate "pesan migration di sini"
  ```

  Edit file migration **jika perlu**, kemudian jalankan migrationnya:

  ```bash
  pipenv run invoke migration.up
  ```

- Untuk menjalankan dengan production environment, jalankan:

  ```bash
  # Jika baru pertama kali menjalankan
  docker-compose up --build

  # Atau jika image sudah pernah di-build sebelumnya
  docker-compose up
  ```
