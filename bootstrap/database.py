import os


URL_PARTS = [
    "DB_ENGINE",
    "DB_DRIVER",
    "DB_USERNAME",
    "DB_PASSWORD",
    "DB_HOST",
    "DB_PORT",
    "DB_NAME",
]


def build_url():
    parts = [os.getenv(part) for part in URL_PARTS]

    return "{}+{}://{}:{}@{}:{}/{}".format(*parts)
