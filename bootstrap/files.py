from pathlib import Path
import os


def create_dirs():
    dirs = [
        os.path.join(os.getenv("STORAGE_PATH"), "images"),
        os.path.join(os.getenv("STORAGE_PATH"), "files"),
        os.getenv("STATIC_PATH"),
    ]

    for dirname in dirs:
        Path(dirname).mkdir(parents=True, exist_ok=True)


def create_symlinks():
    static = os.getenv("STATIC_PATH")
    storage = os.getenv("STORAGE_PATH")

    for name in ["files", "images"]:
        symlink = Path(os.path.join(static, name))
        if not symlink.exists():
            os.symlink(
                os.path.join(storage, name),
                symlink,
                target_is_directory=True,
            )
