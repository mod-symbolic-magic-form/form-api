"""add cover_img column to forms

Revision ID: b8d701ec08b2
Revises: 82160a2b752d
Create Date: 2022-06-24 21:35:15.132071

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b8d701ec08b2'
down_revision = '82160a2b752d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('forms', sa.Column('cover_img', sa.String(length=256), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('forms', 'cover_img')
    # ### end Alembic commands ###
