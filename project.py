import os
import sys

from invoke import Collection

import bootstrap


DIRNAME = os.path.dirname(os.path.realpath(__file__))

GLOBALS = {
    "PROJECT_ROOT"  : DIRNAME,
    "STATIC_PATH"   : os.path.join(DIRNAME, "static"),
    "STORAGE_PATH"  : os.path.join(DIRNAME, "storage"),
    "TESTDATA_PATH" : os.path.join(DIRNAME, "tests", "testdata"),
    "TEMPDIR_PATH"  : os.path.join(DIRNAME, "temp"),
    "DB_URL"        : bootstrap.database.build_url(),
}

# Register global variables as environment variables
os.environ |= GLOBALS

# add src/ to path
sys.path.insert(0, os.path.join(DIRNAME, "src"))

bootstrap.files.create_dirs()
bootstrap.files.create_symlinks()


#####################################################################

from tasks import TASK_LIST

namespace = Collection(*TASK_LIST)
