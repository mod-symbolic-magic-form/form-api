from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from base.model import Model


class Answer(Model):
    __tablename__ = "answers"

    id = Column(Integer, primary_key=True, index=True)
    text = Column(String(256), nullable=False)
    response_id = Column(Integer, ForeignKey("responses.id"), nullable=False)
    question_id = Column(Integer, ForeignKey("questions.id"), nullable=False)

    response = relationship("Response", back_populates="answers")
    question = relationship("Question", back_populates="answers")

    def __repr__(self):
        return (
            f"<Answer #{self.id}: from Response#{self.response_id}"
            f" answering Question#{self.question_id}>"
        )
