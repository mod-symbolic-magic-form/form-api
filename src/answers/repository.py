from sqlalchemy.orm import Session

from base.repository import Repository
from .models import Answer


class AnswerRepository(Repository):
    def __init__(self, db_session: Session):
        super().__init__(db_session, Answer)
