import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from base.model import Model
import answers
import files
import forms
import questions
import responses
import users


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(files.router)
app.include_router(forms.router)
app.include_router(users.router)

app.mount(
    "/static/images",
    StaticFiles(
        directory=os.path.join(os.getenv("STATIC_PATH"), "images"),
    ),
)
