from sqlalchemy import func
from sqlalchemy.orm import Session

from .model import Model


class Repository:
    def __init__(self, db_session: Session, model: Model):
        self.db_session = db_session
        self.model = model

    def get(self, id):
        return self.db_session.query(self.model).filter(self.model.id == id).first()

    def get_all(self):
        return self.db_session.query(self.model).all()

    def commit(self):
        self.db_session.commit()

    def refresh(self, *objs):
        for obj in objs:
            if isinstance(obj, self.model):
                self.db_session.refresh(obj)

    def commit_refresh(self, *objs):
        self.commit()
        self.refresh(*objs)

    def create(self, **attrs):
        entry = self.model(**attrs)

        self.db_session.add(entry)
        self.db_session.commit()
        self.db_session.refresh(entry)

        return entry

    def update(self, id, **attrs):
        entry = self.db_session.query(self.model).filter(self.model.id == id).first()

        for k, v in attrs.items():
            setattr(entry, k, v)

        self.db_session.commit()

    def delete(self, id):
        entry = self.get(id)

        self.db_session.delete(entry)
        self.db_session.commit()

    def delete_all(self):
        self.db_session.query(self.model).delete()
        self.db_session.commit()

    def count(self):
        return self.db_session\
            .query(func.count(self.model.id))\
            .scalar()
