from base.database import AppDatabaseSession


def get_db():
    db = AppDatabaseSession()
    try:
        yield db
    finally:
        db.close()
