from urllib.parse import urlparse

from fastapi import (
    Depends,
    HTTPException,
    Request,
    UploadFile,
)

from users.services import AuthService
from ..router import router
from ..schema import FileUrl as FileUrlSchema
from ..services import ImageService


@router.post(
    "/images",
    status_code=201,
    dependencies=[Depends(AuthService.Http.get_current_user)],
    response_model=FileUrlSchema,
)
async def upload_image(img: UploadFile, request: Request):
    try:
        await img.seek(0)
        img_name = ImageService().store(img)
        await img.close()
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    parsed_url = urlparse(str(request.url))

    return {
        "url": f"{parsed_url.scheme}://{parsed_url.netloc}/static/images/{img_name}"
    }
