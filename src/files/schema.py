from pydantic import BaseModel


class FileUrl(BaseModel):
    url: str
