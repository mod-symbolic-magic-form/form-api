import os
import secrets
import shutil

from fastapi import UploadFile


class ImageService:
    @staticmethod
    def is_accepting_content_type(content_type: str):
        return content_type in ["image/jpeg", "image/png"]


    def store(self, img: UploadFile):
        if not ImageService.is_accepting_content_type(img.content_type):
            raise Exception(f"Unsupported MIME type: {img.content_type}")

        ext = img.content_type.split("/")[1]

        return ImageService.Image(ext).create_from(img)


    class Image:
        def __init__(self, ext: str):
            self.hash = secrets.token_urlsafe(12)
            self.ext = ext
            self.name = f"{self.hash}.{self.ext}"
            self.path = os.path.join(os.getenv("STORAGE_PATH"), "images", self.name)

        def create_from(self, src: UploadFile):
            with open(self.path, "xb") as f:
                shutil.copyfileobj(src.file, f)

            return self.name
