from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session

from dependencies import get_db
from responses.repository import ResponseRepository
from users.repository import UserRepository
from users.schema import User as UserSchema
from users.services import AuthService
from ..repository import FormRepository
from ..router import router
from ..schema import FormResponseAnswer


@router.post("/{form_id}/answer", status_code=201)
def answer(
    form_id: str,
    answers: FormResponseAnswer,
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    user = UserRepository(db).get_by_email(user.email)
    form = FormRepository(db).get(form_id)

    if not form:
        raise HTTPException(status_code=404, detail="Form not found")
    elif len(form.questions) != len(answers.answers):
        raise HTTPException(
            status_code=400,
            detail="The number of the answers is not the same with form questions",
        )

    response = ResponseRepository(db).create(user_id=user.id, form_id=form_id)
    response.create_answers(answers.answers)

    ResponseRepository(db).commit_refresh(response)
