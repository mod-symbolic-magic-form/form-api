from fastapi import Depends
from sqlalchemy.orm import Session

from dependencies import get_db
from users.repository import UserRepository
from users.schema import User as UserSchema
from users.services import AuthService
from ..repository import FormRepository
from ..router import router
from ..schema import FormBase as FormBaseSchema


@router.post("/", status_code=201, response_model=FormBaseSchema)
def create(
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user)
):
    user = UserRepository(db).get_by_email(user.email)
    form = FormRepository(db).create(author_id=user.id)

    return {"id": form.id}
