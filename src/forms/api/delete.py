from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session

from dependencies import get_db
from helpers import ListHelper
from users.repository import UserRepository
from users.schema import User as UserSchema
from users.services import AuthService
from ..repository import FormRepository
from ..router import router


@router.delete("/{form_id}")
def delete(
    form_id: str,
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    user = UserRepository(db).get_by_email(user.email)
    form = ListHelper.find(user.forms, lambda f: f.id == form_id)

    if not form:
        raise HTTPException(status_code=404, detail="Form not found")

    FormRepository(db).delete(form_id)
