from fastapi import Depends
from sqlalchemy.orm import Session

from dependencies import get_db
from users.repository import UserRepository
from users.schema import User as UserSchema
from users.services import AuthService
from ..repository import FormRepository
from ..router import router
from ..schema import (
    FormSummary as FormSummarySchema,
    FormSummaryList as FormSummaryListSchema,
)


@router.get("/published", response_model=FormSummaryListSchema)
def get_published_forms(db: Session = Depends(get_db)):
    forms = FormRepository(db).get_published_forms()

    return {
        "forms": list(map(FormSummarySchema.from_orm, forms)),
    }


@router.get("/own", response_model=FormSummaryListSchema)
def get_own_forms(
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    user = UserRepository(db).get_by_email(user.email)

    return {
        "forms": list(map(FormSummarySchema.from_orm, user.forms)),
    }
