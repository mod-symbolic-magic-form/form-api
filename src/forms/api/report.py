from fastapi import (
    Depends,
    HTTPException,
    Response,
)
from sqlalchemy.orm import Session

from dependencies import get_db
from users.repository import UserRepository
from users.schema import User as UserSchema
from users.services import AuthService
from ..repository import FormRepository
from ..router import router
from ..services import (
    FormResponsesService,
    ReportGeneratorService,
)


@router.get("/{form_id}/report", response_class=Response)
def generate_report(
    form_id: str,
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    form = FormRepository(db).get(form_id)
    user = UserRepository(db).get_by_email(user.email)

    if not form:
        raise HTTPException(status_code=404, detail="Form not found")
    elif not form.is_published:
        raise HTTPException(status_code=403, detail="Form is not yet published")
    elif not form.report_can_be_viewed_by(user):
        raise HTTPException(status_code=401, detail=(
            "User does not have the permission to access the form"
        ))

    data = FormResponsesService.serialize(form)
    filebytes = ReportGeneratorService(data).generate()

    return Response(
        content=filebytes.getvalue(),
        media_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )
