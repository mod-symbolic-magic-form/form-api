from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session

from dependencies import get_db
from users.repository import UserRepository
from users.schema import User as UserSchema
from users.services import AuthService
from ..repository import FormRepository
from ..router import router
from ..schema import (
    Form as FormSchema,
    FormWithQuestions as FormWithQuestionsSchema,
)


@router.get("/{form_id}", response_model=FormWithQuestionsSchema)
def show(
    form_id: str,
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    form = FormRepository(db).get(form_id)
    user = UserRepository(db).get_by_email(user.email)

    if not form:
        raise HTTPException(status_code=404, detail="Form not found")
    elif not form.is_published and form.author != user:
        # prevent other users from accessing
        # unpublished forms
        raise HTTPException(status_code=401, detail=(
            "User does not have the permission to access the form"
        ))

    form_schema = FormSchema.from_orm(form)
    questions = list(map(lambda q: q["text"], form.get_sorted_questions()))

    # combine the dicts
    return form_schema.dict() | { "questions": questions }
