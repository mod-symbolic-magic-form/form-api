from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session

from dependencies import get_db
from helpers import DictHelper, ListHelper
from users.repository import UserRepository
from users.schema import User as UserSchema
from users.services import AuthService
from ..repository import FormRepository
from ..router import router
from ..schema import (
    FormAppearanceUpdate as FormAppearanceUpdateSchema,
    FormSettingUpdate as FormSettingUpdateSchema,
    FormQuestionsUpdate as FormQuestionsUpdateSchema,
)


@router.patch("/{form_id}/appearance")
def update_appearance(
    form_id: str,
    updates: FormAppearanceUpdateSchema,
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    user = UserRepository(db).get_by_email(user.email)
    form = ListHelper.find(user.forms, lambda f: f.id == form_id)

    if not form:
        raise HTTPException(status_code=404, detail="Form not found")

    updates = DictHelper.filter_values(
        updates.dict(),
        lambda val: val,
    )

    FormRepository(db).update(form_id, **updates)


@router.patch("/{form_id}/settings")
def update_settings(
    form_id: str,
    updates: FormSettingUpdateSchema,
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    user = UserRepository(db).get_by_email(user.email)
    form = ListHelper.find(user.forms, lambda f: f.id == form_id)

    if not form:
        raise HTTPException(status_code=404, detail="Form not found")

    updates = DictHelper.filter_values(
        updates.dict(),
        lambda val: val,
    )

    FormRepository(db).update(form_id, **updates)


@router.put("/{form_id}/questions")
def update_questions(
    form_id: str,
    updates: FormQuestionsUpdateSchema,
    db: Session = Depends(get_db),
    user: UserSchema = Depends(AuthService.Http.get_current_user),
):
    user = UserRepository(db).get_by_email(user.email)
    form = ListHelper.find(user.forms, lambda f: f.id == form_id)

    if not form:
        raise HTTPException(status_code=404, detail="Form not found")
    elif form.is_published:
        raise HTTPException(status_code=403, detail="Form is already published")

    form.remove_questions()
    form.create_questions(updates.questions)

    FormRepository(db).commit_refresh(form)
