class VisualizationPermission:
    private = 0
    public = 1

    @staticmethod
    def from_str(s: str):
        if s not in ["private", "public"]:
            raise ValueError(f"Invalid permission: {s}")

        return getattr(VisualizationPermission, s)
