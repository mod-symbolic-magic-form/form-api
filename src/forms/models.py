import uuid

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from base.model import Model
from questions.models import Question
from users.models import User
from .enums import VisualizationPermission


class Form(Model):
    __tablename__ = "forms"

    id = Column(
        String(36),
        primary_key=True,
        index=True,
        default=lambda:str(uuid.uuid4()),
    )
    title = Column(String(256))
    theme_header = Column(String(60))
    theme_background = Column(String(60))
    cover_img = Column(String(256))
    is_published = Column(Boolean, nullable=False, default=False)
    data_permission = Column(
        Integer,
        nullable=False,
        default=VisualizationPermission.private,
    )
    author_id = Column(Integer, ForeignKey("users.id"), nullable=False)

    author = relationship("User", back_populates="forms")
    questions = relationship(
        "Question",
        back_populates="form",
        cascade="all, delete-orphan",
    )
    responses = relationship(
        "Response",
        back_populates="form",
        cascade="all, delete-orphan",
    )

    def __repr__(self):
        return f"<Form #{self.id}>"

    def report_can_be_viewed_by(self, user: User):
        return (
            self.data_permission == VisualizationPermission.public \
            or user == self.author)

    def get_sorted_questions(self):
        questions = list(map(lambda q: (q.id, q.text, q.ordering), self.questions))
        questions.sort(key=lambda q: q[2])

        return list(map(
            lambda q: {
                "id": q[0],
                "text": q[1],
            },
            questions
        ))

    def create_questions(self, questions: list[str]):
        for i, q in enumerate(questions):
            self.questions.append(Question(
                text=q,
                ordering=i,
            ))

    def remove_questions(self):
        self.questions[:] = []
