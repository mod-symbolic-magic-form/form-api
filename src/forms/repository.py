from sqlalchemy.orm import Session

from base.repository import Repository
from questions.repository import QuestionRepository
from .models import Form


class FormRepository(Repository):
    def __init__(self, db_session: Session):
        super().__init__(db_session, Form)

    def get_published_forms(self):
        return self.db_session\
            .query(self.model)\
            .filter(self.model.is_published)\
            .all()

    def commit(self):
        super().commit()
        QuestionRepository(self.db_session).delete_removed_questions()
