from pathlib import Path
from typing import List
from urllib.parse import urlparse
import os

from pydantic import BaseModel, validator

from responses.schema import ResponseSimple
from .enums import VisualizationPermission


class FormBase(BaseModel):
    id: str


class Form(FormBase):
    title: str | None = None
    cover_img: str | None = None
    theme_header: str | None = None
    theme_background: str | None = None
    is_published: bool
    data_permission: int

    class Config:
        orm_mode = True


class FormWithQuestions(Form):
    questions: List[str]


class FormSummary(FormBase):
    title: str | None = None
    cover_img: str | None = None

    class Config:
        orm_mode = True


class FormSummaryList(BaseModel):
    forms: list[FormSummary]


class FormAppearanceUpdate(BaseModel):
    cover_img: str | None = None
    theme_header: str | None = None
    theme_background: str | None = None

    @validator("cover_img")
    def image_must_exists(cls, url):
        if url:
            img_name = Path(urlparse(url).path).name
            img_path = os.path.join(os.getenv("STATIC_PATH"), "images", img_name)

            if not Path(img_path).is_file():
                raise ValueError("Image does not exist")

        return url


class FormSettingUpdate(BaseModel):
    title: str | None = None
    is_published: bool | None = None
    data_permission: str | None = None

    @validator("data_permission")
    def valid_data_sharing_permission(cls, permission):
        if permission:
            permission = VisualizationPermission.from_str(permission)

        return permission


class FormQuestionsUpdate(BaseModel):
    questions: List[str]


class FormResponseAnswer(BaseModel):
    answers: List[str]


class FormSerializedResponses(BaseModel):
    title: str | None = None
    questions: list[str]
    responses: list[ResponseSimple]
