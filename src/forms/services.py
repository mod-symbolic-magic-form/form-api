from io import BytesIO
import xlsxwriter as xw

from .models import Form
from .schema import FormSerializedResponses


class FormResponsesService:
    @staticmethod
    def serialize(form: Form) -> FormSerializedResponses:
        return FormSerializedResponses(
            title=form.title,
            questions=list(map(
                lambda q: q["text"],
                form.get_sorted_questions(),
            )),
            responses=[
                r.get_simple_response() for r in form.responses
            ],
        )


class ReportGeneratorService:
    def __init__(self, data: FormSerializedResponses):
        self.data = data
        self.workbook = None
        self.worksheet = None

    def generate(self) -> BytesIO:
        output = BytesIO()
        self.workbook = xw.Workbook(output, {"in_memory": True})
        self.worksheet = self.workbook.add_worksheet()

        self.__create_header()
        self.__fill_rows()
        self.workbook.close()

        return output

    def __create_header(self):
        num_questions = len(self.data.questions)

        self.worksheet.merge_range("A1:A2", "No")
        self.worksheet.merge_range("B1:B2", "Username")
        self.worksheet.merge_range(
            0, 2, # C1
            0, 2 + num_questions - 1, # ?1
            "Questions",
        )
        self.worksheet.write_row(
            "C2",
            [i + 1 for i in range(num_questions)],
        )

    def __fill_rows(self):
        # entries start from A3
        row = 3
        for i, resp in enumerate(self.data.responses):
            entry = [i + 1, resp.respondent] + resp.answers
            self.worksheet.write_row(
                f"A{row}",
                entry,
            )
            row += 1
