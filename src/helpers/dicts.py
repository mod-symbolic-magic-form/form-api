class DictHelper:
    @staticmethod
    def filter_values(d: dict, fn):
        return {k: v for k, v in d.items() if fn(v)}
