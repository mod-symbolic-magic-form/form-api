class ListHelper:
    @staticmethod
    def find(ls: list, fn):
        for el in ls:
            if fn(el):
                return el
        return None
