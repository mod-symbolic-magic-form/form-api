from users.services import AuthService


class TestHelper:
    @staticmethod
    def create_user_data(name: str) -> dict[str, str]:
        return {
            "email": f"{name}@email.com",
            "name": name,
            "password": name,
        }

    @staticmethod
    def create_access_token(name: str, email: str, **_) -> str:
        return AuthService.create_token({
            "email": email,
            "name": name,
        })["access_token"]
