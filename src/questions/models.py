from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from base.model import Model


class Question(Model):
    __tablename__ = "questions"

    id = Column(Integer, primary_key=True, index=True)
    text = Column(String(256), nullable=False)
    ordering = Column(Integer, nullable=False)
    form_id = Column(String(36), ForeignKey("forms.id"))

    form = relationship("Form", back_populates="questions")
    answers = relationship(
        "Answer",
        back_populates="question",
        cascade="all, delete-orphan",
    )

    def __repr__(self):
        return f"<Question #{self.id}: {self.text}>"
