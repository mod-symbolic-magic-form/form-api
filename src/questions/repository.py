from sqlalchemy.orm import Session

from base.repository import Repository
from .models import Question


class QuestionRepository(Repository):
    def __init__(self, db_session: Session):
        super().__init__(db_session, Question)

    def delete_removed_questions(self):
        questions = self.db_session\
            .query(self.model)\
            .filter(self.model.form_id == None)\
            .all()

        for q in questions:
            self.delete(q.id)
