from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from answers.models import Answer
from base.model import Model


class Response(Model):
    __tablename__ = "responses"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    form_id = Column(String(36), ForeignKey("forms.id"), nullable=False)

    respondent = relationship("User", back_populates="responses")
    form = relationship("Form", back_populates="responses")
    answers = relationship(
        "Answer",
        back_populates="response",
        cascade="all, delete-orphan",
    )

    def __repr__(self):
        return f"<Response #{self.id}: by User#{self.user_id} to Form#{self.form_id}>"

    def create_answers(self, answers: list[str]):
        questions = self.form.get_sorted_questions()
        for i, q in enumerate(questions):
            self.answers.append(Answer(
                text=answers[i],
                question_id=q["id"],
            ))

    def get_simple_response(self):
        return {
            "respondent": self.respondent.name,
            "answers": list(map(
                lambda a: a.text,
                self.answers,
            ))
        }
