from sqlalchemy.orm import Session

from answers.repository import AnswerRepository
from base.repository import Repository
from forms.repository import FormRepository
from .models import Response


class ResponseRepository(Repository):
    def __init__(self, db_session: Session):
        super().__init__(db_session, Response)
