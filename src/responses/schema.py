from pydantic import BaseModel


class ResponseSimple(BaseModel):
    respondent: str
    answers: list[str]
