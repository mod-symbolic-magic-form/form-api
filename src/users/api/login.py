from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from dependencies import get_db
from ..router import router
from ..schema import Token as TokenSchema
from ..services import AuthService


@router.post("/login", response_model=TokenSchema)
def login(
    db: Session = Depends(get_db),
    form: OAuth2PasswordRequestForm = Depends(),
):
    try:
        user = AuthService(db).verify_user(form)
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

    return AuthService.create_token({
        "email": user.email,
        "name": user.name
    })
