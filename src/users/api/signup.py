from fastapi import Depends, HTTPException, Response
from sqlalchemy.orm import Session

from dependencies import get_db
from ..repository import UserRepository
from ..router import router
from ..schema import (
    Token as TokenSchema,
    UserCreate as SignupSchema,
)
from ..services import AuthService


@router.post("/signup", response_model=TokenSchema)
def signup(
    user: SignupSchema,
    response: Response,
    db: Session = Depends(get_db),
):
    try:
        user = UserRepository(db).create(**user.dict())
        response.status_code = 201
    except:
        raise HTTPException(status_code=400, detail="Email already registered")

    return AuthService.create_token({
        "email": user.email,
        "name": user.name,
    })
