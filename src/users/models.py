from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from base.model import Model


class User(Model):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String(256), unique=True, index=True)
    name = Column(String(60), unique=True, index=True)
    password_hash = Column(String(72))
    avatar_link = Column(String(256))

    forms = relationship("Form", back_populates="author", cascade="all, delete-orphan")
    responses = relationship(
        "Response",
        back_populates="respondent",
        cascade="all, delete-orphan",
    )

    def __repr__(self):
        return f"<User #{self.id}: {self.email}>"
