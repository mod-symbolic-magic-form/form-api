from passlib.context import CryptContext
from sqlalchemy.orm import Session

from base.repository import Repository
from .models import User


class UserRepository(Repository):
    def __init__(self, db_session: Session):
        super().__init__(db_session, User)

        self.password_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def get_by_email(self, email: str):
        return self.db_session.query(self.model).filter(self.model.email == email).first()

    def create(self, **attrs):
        if self.get_by_email(attrs["email"]):
            raise Exception("User already exists")

        attrs["password_hash"] = self.password_context.hash(attrs.pop("password"))

        return super().create(**attrs)
