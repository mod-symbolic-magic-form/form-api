import os
#from datetime import datetime, timedelta

from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from .repository import UserRepository
from .schema import User


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/users/login")


class AuthService:
    @staticmethod
    def create_token(data: dict):
        data = data.copy()
        #data["exp"] = datetime.utcnow() + timedelta(minutes=15)

        return {
            "access_token": jwt.encode(
                data,
                os.getenv("APP_SECRET_KEY"),
                algorithm=os.getenv("APP_JWT_ALG"),
            ),
            "token_type": "Bearer",
        }

    @staticmethod
    def verify_token(token: str):
        return jwt.decode(
            token,
            os.getenv("APP_SECRET_KEY"),
            algorithms=[os.getenv("APP_JWT_ALG")],
        )


    def __init__(self, db_session: Session):
        self.repository = UserRepository(db_session)
        self.password_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def verify_user(self, credentials: OAuth2PasswordRequestForm):
        user = self.repository.get_by_email(credentials.username)

        if not user:
            raise Exception("User does not exist")
        if not self.password_context.verify(credentials.password, user.password_hash):
            raise Exception("Password mismatch")

        return user


    class Http:
        @staticmethod
        def get_current_user(token: str = Depends(oauth2_scheme)):
            try:
                return User(**AuthService.verify_token(token))
            except:
                raise HTTPException(status_code=401, detail="Invalid token")
