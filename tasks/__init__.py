from . import migration, server


TASK_LIST = [
    server.dev,
    server.prod,
    server.test,
    migration,
]
