import os

from alembic.config import Config
from invoke import task
import alembic


config = Config(os.path.join(os.getenv("PROJECT_ROOT"), "alembic.ini"))


@task(help={
    "message": "Migration revision message",
})
def generate(c, message):
    """
    Detect changes from each registered model and create a new revision
    """
    alembic.command.revision(config, message=message, autogenerate=True)


@task
def up(c):
    """
    Run migrations to the latest version
    """
    alembic.command.upgrade(config, "head")


@task
def down(c):
    """
    Rollback migrations
    """
    alembic.command.downgrade(config, "base")
