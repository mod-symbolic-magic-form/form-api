import os
import platform
import sys

from invoke import task
import alembic
import uvicorn

from . import migration
import bootstrap


@task(help={
    "reload": "Run server in hot-reload mode",
})
def dev(c, reload=False):
    """
    Run development server
    """
    uvicorn.run("app:app", reload=reload)


@task
def prod(c):
    """
    Run server on production environment
    """
    if platform.system() != "Linux":
        print("unsupported platform", file=sys.stderr)
        sys.exit(1)

    from app import app
    import gunicorn.app.base

    class Application(gunicorn.app.base.BaseApplication):
        def __init__(self, app):
            self.application = app
            self.options = {
                "bind": "0.0.0.0:8000",
                "workers": 4,
                "worker_class": "uvicorn.workers.UvicornWorker",
            }
            super().__init__()

        def load_config(self):
            for key, val in self.options.items():
                if key in self.cfg.settings and val is not None:
                    self.cfg.set(key.lower(), val)

        def load(self):
            return self.application

    Application(app).run()


@task
def test(c):
    """
    Run tests
    """
    import pytest

    # overwrite database configs
    for part in bootstrap.database.URL_PARTS:
        os.environ[part] = os.getenv(f"TEST_{part}")

    os.environ["DB_URL"] = bootstrap.database.build_url()

    # setup test database
    alembic.command.upgrade(migration.config, "head")

    pytest.main(["tests/", "-W", "ignore:Module already imported:pytest.PytestWarning"])

    # cleanup
    alembic.command.downgrade(migration.config, "base")
