from urllib.parse import urlparse
import os
import unittest

from fastapi.testclient import TestClient

from app import app
from base.database import AppDatabaseSession
from helpers import TestHelper
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")


class TestImagesUpload(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.target_path = "/files/images"
        cls.db = AppDatabaseSession()

        UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        self.img_url = None

    def tearDown(self):
        if self.img_url:
            parsed = urlparse(self.img_url)
            img_path = os.path.join(
                os.getenv("PROJECT_ROOT"),
                *parsed.path.split("/")[1:],
            )
            os.remove(img_path)


    def test_image_upload(self):
        f = os.path.join(os.getenv("TESTDATA_PATH"), "cat.png")

        response = self.client.post(
            self.target_path,
            files={"img": ("cat.png", open(f, "rb"), "image/png")},
            headers={"Authorization": f"Bearer {self.alice_token}"},
        )

        assert response.status_code == 201
        assert "url" in response.json()

        self.img_url = response.json()["url"]


    def test_unauthorized_image_upload(self):
        response = self.client.post(self.target_path)

        assert response.status_code == 401


    def test_unsupported_content_type(self):
        f = os.path.join(os.getenv("TESTDATA_PATH"), "empty.txt")

        response = self.client.post(
            self.target_path,
            files={"img": ("empty.txt", open(f, "rb"), "plain/text")},
            headers={"Authorization": f"Bearer {self.alice_token}"},
        )

        assert response.status_code == 400
