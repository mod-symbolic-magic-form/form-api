import unittest

from fastapi.testclient import TestClient

from answers.repository import AnswerRepository
from app import app
from base.database import AppDatabaseSession
from forms.repository import FormRepository
from helpers import TestHelper
from questions.repository import QuestionRepository
from responses.repository import ResponseRepository
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")


class TestFormAnswer(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/{form_id}/answer"

        cls.user = UserRepository(cls.db).create(**ALICE)
        cls.access_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        self.form = FormRepository(self.db).create(author_id=self.user.id)
        self.form.create_questions([
            "Hello, how are you?",
            "What is your name?",
        ])

        FormRepository(self.db).commit_refresh(self.form)

    def tearDown(self):
        AnswerRepository(self.db).delete_all()
        ResponseRepository(self.db).delete_all()
        QuestionRepository(self.db).delete_all()
        FormRepository(self.db).delete_all()


    def test_form_answer(self):
        response = self.client.post(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.access_token}"},
            json={
                "answers": [
                    "I'm good",
                    "My name is snaztoz",
                ],
            },
        )

        self.db.commit()

        assert response.status_code == 201
        assert len(AnswerRepository(self.db).get_all()) == 2
