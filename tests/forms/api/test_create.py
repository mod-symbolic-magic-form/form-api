import unittest

from fastapi.testclient import TestClient

from app import app
from base.database import AppDatabaseSession
from forms.repository import FormRepository
from helpers import TestHelper
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")


class TestFormCreate(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/"

        UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def tearDown(self):
        FormRepository(self.db).delete_all()


    def test_form_create(self):
        response = self.client.post(self.target_path, headers={
            "Authorization": f"Bearer {self.alice_token}"
        })

        assert response.status_code == 201
        assert "id" in response.json()


    def test_unauthorized_form_create(self):
        response = self.client.post(self.target_path)

        assert response.status_code == 401
