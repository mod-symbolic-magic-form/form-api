import unittest

from fastapi.testclient import TestClient

from app import app
from base.database import AppDatabaseSession
from forms.repository import FormRepository
from helpers import TestHelper
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")
BOB = TestHelper.create_user_data("bob")


class TestFormDelete(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/{form_id}"

        cls.alice = UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        self.form = FormRepository(self.db).create(author_id=self.alice.id)

    def tearDown(self):
        FormRepository(self.db).delete_all()


    def test_form_delete(self):
        response = self.client.delete(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"}
        )

        assert response.status_code == 200


    def test_delete_non_existing_form(self):
        response = self.client.delete(
            "/forms/non-existing-form-id",
            headers={"Authorization": f"Bearer {self.alice_token}"}
        )

        assert response.status_code == 404


    def test_unauthorized_form_delete(self):
        bob = UserRepository(self.db).create(**BOB)
        bob_token = TestHelper.create_access_token(**BOB)

        response = self.client.delete(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {bob_token}"}
        )

        assert response.status_code == 404
