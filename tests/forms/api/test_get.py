import unittest

from fastapi.testclient import TestClient

from app import app
from base.database import AppDatabaseSession
from forms.repository import FormRepository
from helpers import TestHelper
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")


class TestGetPublishedForms(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/published"

        cls.alice = UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        # unpublished form
        FormRepository(self.db).create(author_id=self.alice.id)

        # published form
        pub = FormRepository(self.db).create(author_id=self.alice.id)
        pub.is_published = True

        self.db.commit()

    def tearDown(self):
        FormRepository(self.db).delete_all()


    def test_get_published_forms(self):
        response = self.client.get(self.target_path)

        assert response.status_code == 200
        assert len(response.json()["forms"]) == 1


class TestGetOwnForms(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/own"

        cls.alice = UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        # unpublished form
        FormRepository(self.db).create(author_id=self.alice.id)

    def tearDown(self):
        FormRepository(self.db).delete_all()


    def test_form_show(self):
        response = self.client.get(
            self.target_path,
            headers={"Authorization": f"Bearer {self.alice_token}"}
        )

        assert response.status_code == 200
        assert len(response.json()["forms"]) == 1
