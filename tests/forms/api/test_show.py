import unittest

from fastapi.testclient import TestClient

from app import app
from base.database import AppDatabaseSession
from forms.repository import FormRepository
from helpers import TestHelper
from questions.repository import QuestionRepository
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")
BOB = TestHelper.create_user_data("bob")


class TestFormShow(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/{form_id}"

        cls.author = UserRepository(cls.db).create(**ALICE)
        cls.respondent = UserRepository(cls.db).create(**BOB)

        cls.bob_token = TestHelper.create_access_token(**BOB)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        self.form = FormRepository(self.db).create(author_id=self.author.id)
        self.form.create_questions([
            "hi, how are you?",
            "what is your name?",
        ])

        FormRepository(self.db).commit_refresh(self.form)

    def tearDown(self):
        QuestionRepository(self.db).delete_all()
        FormRepository(self.db).delete_all()


    def test_form_show(self):
        self.form.is_published = True
        self.db.commit()
        self.db.refresh(self.form)

        response = self.client.get(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.bob_token}"}
        )

        assert response.status_code == 200
        assert len(response.json()["questions"]) == 2


    def test_prevent_showing_unpublished_form(self):
        response = self.client.get(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.bob_token}"}
        )

        assert response.status_code == 401


    def test_showing_unpublished_form_to_its_author(self):
        alice_token = TestHelper.create_access_token(**ALICE)

        response = self.client.get(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {alice_token}"}
        )

        assert response.status_code == 200
