import os
import shutil
import unittest

from fastapi.testclient import TestClient

from app import app
from base.database import AppDatabaseSession
from forms.enums import VisualizationPermission
from forms.repository import FormRepository
from helpers import TestHelper
from questions.repository import QuestionRepository
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")


class TestFormAppearanceUpdate(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/{form_id}/appearance"

        cls.alice = UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

        shutil.copyfile(
            os.path.join(os.getenv("TESTDATA_PATH"), "cat.png"),
            os.path.join(os.getenv("STATIC_PATH"), "images", "form_update_cat.png"),
        )

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

        os.remove(
            os.path.join(os.getenv("STATIC_PATH"), "images", "form_update_cat.png"),
        )

    def setUp(self):
        self.form = FormRepository(self.db).create(author_id=self.alice.id)

    def tearDown(self):
        FormRepository(self.db).delete_all()


    def test_update_form(self):
        response = self.client.patch(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"},
            json={
                "cover_img": "/static/images/form_update_cat.png",
                "theme_background": "red",
            },
        )

        assert response.status_code == 200


    def test_update_form_non_existing_image(self):
        response = self.client.patch(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"},
            json={
                "cover_img": "/static/images/non_existing.png",
            },
        )

        assert response.status_code == 422


class TestFormSettingsUpdate(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/{form_id}/settings"

        cls.alice = UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        self.form = FormRepository(self.db).create(author_id=self.alice.id)

    def tearDown(self):
        FormRepository(self.db).delete_all()


    def test_update_form(self):
        response = self.client.patch(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"},
            json={
                "title": "My Simple Form",
                "is_published": True,
            },
        )

        self.db.commit()
        self.db.refresh(self.form)

        assert response.status_code == 200
        assert self.form.title == "My Simple Form"
        assert self.form.is_published
        assert self.form.data_permission == VisualizationPermission.private


    def test_update_form_invalid_permission(self):
        response = self.client.patch(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"},
            json={
                "data_permission": "lol_unknown_permission",
            },
        )

        assert response.status_code == 422


class TestFormQuestionsUpdate(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/forms/{form_id}/questions"

        cls.alice = UserRepository(cls.db).create(**ALICE)
        cls.alice_token = TestHelper.create_access_token(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def setUp(self):
        self.form = FormRepository(self.db).create(author_id=self.alice.id)

    def tearDown(self):
        QuestionRepository(self.db).delete_all()
        FormRepository(self.db).delete_all()


    def test_update_form_questions(self):
        self.client.put(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"},
            json={
                "questions": ["This question will be deleted later"],
            },
        )

        response = self.client.put(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"},
            json={
                "questions": ["Hello, how are you?"],
            },
        )

        self.db.commit()
        self.db.refresh(self.form)

        assert self.form.questions[0].text == "Hello, how are you?"


    def test_update_published_post_questions(self):
        self.form.is_published = True
        self.db.commit()

        response = self.client.put(
            self.target_path.format(form_id=self.form.id),
            headers={"Authorization": f"Bearer {self.alice_token}"},
            json={
                "questions": ["Hello, how are you?"],
            },
        )

        assert response.status_code == 403
