import unittest

from app import app
from base.database import AppDatabaseSession
from forms.enums import VisualizationPermission
from forms.repository import FormRepository
from helpers import TestHelper
from questions.repository import QuestionRepository
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")
BOB = TestHelper.create_user_data("bob")


class TestFormModel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.db = AppDatabaseSession()
        cls.alice = UserRepository(cls.db).create(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()

    def tearDown(self):
        QuestionRepository(self.db).delete_all()
        FormRepository(self.db).delete_all()


    def test_report_access(self):
        form1 = FormRepository(self.db).create(author_id=self.alice.id)
        form2 = FormRepository(self.db).create(
            author_id=self.alice.id,
            data_permission=VisualizationPermission.public,
        )

        bob = UserRepository(self.db).create(**BOB)

        # private report permission
        assert form1.report_can_be_viewed_by(self.alice)
        assert not form1.report_can_be_viewed_by(bob)

        # public report permission
        assert form2.report_can_be_viewed_by(self.alice)
        assert form2.report_can_be_viewed_by(bob)


    def test_create_questions(self):
        form = FormRepository(self.db).create(author_id=self.alice.id)
        form.create_questions([
            "How do you do?",
            "What is your name?",
            "How old are you?",
        ])

        FormRepository(self.db).commit_refresh(form)

        assert len(form.questions) == 3

        orderings = list(map(lambda q: q.ordering, form.questions))
        assert orderings == [0, 1, 2]


    def test_delete_questions(self):
        form1 = FormRepository(self.db).create(author_id=self.alice.id)
        form2 = FormRepository(self.db).create(author_id=self.alice.id)

        form1.create_questions(["Foo foo foo?"])
        form2.create_questions([
            "How do you do?",
            "What is your name?",
            "How old are you?",
        ])

        form2.remove_questions()

        FormRepository(self.db).commit_refresh(form1, form2)

        assert len(form1.questions) == 1
        assert len(form2.questions) == 0

        # assert that the removed questions are actually
        # deleted from the tables
        assert QuestionRepository(self.db).count() == 1
