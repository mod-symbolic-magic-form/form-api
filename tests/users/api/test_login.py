import os
import unittest

from fastapi.testclient import TestClient
from jose import jwt

from app import app
from base.database import AppDatabaseSession
from helpers import TestHelper
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")


class TestLogin(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/users/login"

        UserRepository(cls.db).create(**ALICE)

    @classmethod
    def tearDownClass(cls):
        UserRepository(cls.db).delete_all()
        cls.db.close()


    def test_login(self):
        response = self.client.post(self.target_path, data={
            "username": ALICE["email"],
            "password": ALICE["password"],
        })
        assert response.status_code == 200

        data = jwt.decode(
            response.json()["access_token"],
            os.getenv("APP_SECRET_KEY"),
            algorithms=[os.getenv("APP_JWT_ALG")],
        )

        assert data["email"] == ALICE["email"]
        assert data["name"] == ALICE["name"]


    def test_failed_login(self):
        # Test login for non-existing user

        response = self.client.post(self.target_path, data={
            "username": "not_exist@email.com",
            "password": "not_exist_password",
        })
        assert response.status_code == 400
        assert response.json()["detail"] == "User does not exist"

        # Test wrong password

        response = self.client.post(self.target_path, data={
            "username": ALICE["email"],
            "password": "wrong_password",
        })
        assert response.status_code == 400
        assert response.json()["detail"] == "Password mismatch"
