import os
import unittest

from fastapi.testclient import TestClient
from jose import jwt

from app import app
from base.database import AppDatabaseSession
from helpers import TestHelper
from users.repository import UserRepository


ALICE = TestHelper.create_user_data("alice")


class TestSignup(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = TestClient(app)
        cls.db = AppDatabaseSession()
        cls.target_path = "/users/signup"

    @classmethod
    def tearDownClass(cls):
        cls.db.close()

    def tearDown(self):
        UserRepository(self.db).delete_all()


    def test_signup(self):
        response = self.client.post(self.target_path, json=ALICE)
        assert response.status_code == 201

        data = jwt.decode(
            response.json()["access_token"],
            os.getenv("APP_SECRET_KEY"),
            algorithms=[os.getenv("APP_JWT_ALG")],
        )

        assert data["email"] == ALICE["email"]
        assert data["name"] == ALICE["name"]


    def test_failed_signup(self):
        UserRepository(self.db).create(**ALICE)

        response = self.client.post(self.target_path, json=ALICE)
        assert response.status_code == 400
        assert response.json()["detail"] == "Email already registered"
